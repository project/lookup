<?php

/**
 * @file
 */

function lookup_admin_overview() {
  $query = db_select('lookup_type', 'l')
    ->fields('l', array('lid', 'name', 'type', 'description'))
    ->execute();
  
  $header = array(t('Lookup tables'), array('data' => t('Operations')));
  $rows = array();
  
  foreach ($query as $lookup) {
    $row = array(theme('lookup_admin_overview_details', array('name' => check_plain($lookup->name), 'type' => $lookup->type, 'description' => filter_xss_admin($lookup->description))));
    $row[] = array('data' => l(t('edit'), 'admin/structure/lookup/manage/' . str_replace('_', '-', $lookup->type)));
    
    $rows[] = $row;
  }
  
  $build['lookup_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No lookup tables available'),
  );
  
  return $build;
}

function lookup_admin_form($form, &$form_state, $lookup = NULL) {
  $form = array();
  
  if (!$lookup) {
    $lookup = new stdClass;
    $lookup->name = '';
    $lookup->description = '';
  }
  
  if (isset($lookup->lid)) {
    $form['lid'] = array(
      '#type' => 'value',
      '#value' => $lookup->lid,
    );
  }
  if (isset($lookup->type)) {
    $form['type'] = array(
      '#type' => 'value',
      '#value' => $lookup->type,
    );
  }
  
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $lookup->name,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $lookup->description,
    '#required' => TRUE,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => empty($lookup->lid) ? t('Create') : t('Save'),
  );
  
  return $form;
}

function lookup_admin_form_submit(&$form, &$form_state) {
  if (!isset($form_state['values']['type'])) {
    $form_state['values']['type'] = check_plain(str_replace(' ', '_', drupal_strtolower(trim($form_state['values']['name']))));
  }
  if (isset($form_state['values']['lid'])) {
    drupal_write_record('lookup_type', $form_state['values'], 'lid');
  }
  else {
    drupal_write_record('lookup_type', $form_state['values']);
  }
  
  $form_state['redirect'] = 'admin/structure/lookup';
}